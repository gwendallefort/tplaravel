<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('links')->insert([
            'url' => "https://www.xefi.fr/",
            'titre' => "Site web de Xefi",
            'url_img' => "img/xefi.png",
            'description' => "bla bla",
            'categorie_id' => 1,
        ]);

        DB::table('links')->insert([
            'url' => "https://laravel.com/docs/8.x/blade#introduction",
            'titre' => "doc laravel",
            'url_img' => "img/z1.png",
            'description' => "Vroum vroum la voiture",
            'categorie_id' => 3,
        ]);



        DB::table('links')->insert([
            'url' => "https://laravel.com/docs/8.x/blade#introduction",
            'titre' => "doc laravel",
            'url_img' => "img/z1.png",
            'description' => "Vroum vroum la voiture",
            'categorie_id' => 2,
        ]);

        DB::table('links')->insert([
            'url' => "https://laravel.com/docs/8.x/blade#introduction",
            'titre' => "doc laravel",
            'url_img' => "img/z1.png",
            'description' => "Vroum vroum la voiture",
            'categorie_id' => 3,
        ]);

        DB::table('links')->insert([
            'url' => "https://laravel.com/docs/8.x/blade#introduction",
            'titre' => "doc laravel",
            'url_img' => "img/z1.png",
            'description' => "Vroum vroum la voiture",
            'categorie_id' => 2,
        ]);
        DB::table('links')->insert([
            'url' => "https://laravel.com/docs/8.x/blade#introduction",
            'titre' => "doc laravel",
            'url_img' => "img/z1.png",
            'description' => "Vroum vroum la voiture",
            'categorie_id' => 3,
        ]);
        DB::table('links')->insert([
            'url' => "https://laravel.com/docs/8.x/blade#introduction",
            'titre' => "doc laravel",
            'url_img' => "img/z1.png",
            'description' => "Vroum vroum la voiture",
            'categorie_id' => 3,
        ]);

    }
}
