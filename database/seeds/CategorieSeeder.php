<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Interne",
            "BV",
            "Autres",
        ];

        foreach ($categories as $categorie) {

            DB::table('categories')->insert([
                'type' => $categorie,
            ]);
        }
    }
}
