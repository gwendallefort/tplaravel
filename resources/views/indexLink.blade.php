<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
    </head>
    <body>

    <button>Ajouter un lien</button>

    @foreach($data as $item)

        <h2>{{ $item["titre"] }}</h2>

        <div class="divCategorie">

            @foreach($item["data"] as $link)

                <div class="divLink">
                    <h3>{{$link->titre}}</h3>
                    <a href="{{$link->url}}">
                    <img src="{{url($link->url_img)}}" />
                        <p>{{$link->description}}</p>
                    </a>

                    <form method="POST" action="{{ url('deleteLink/'.$link->id) }}">
                        @csrf
                        <button type="submit">Supprimer</button>
                    </form>

                </div>
            @endforeach

        </div>

    @endforeach


    </body>
</html>
