<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Link;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class LinkController extends Controller
{

    public function index() {

        $categories = Categorie::get();

        $data = [];

        foreach ($categories as $categorie) {
            $data[$categorie->id]["data"] = Link::get()->where("categorie_id", $categorie->id);
            $data[$categorie->id]["titre"] = $categorie->type;
        }

        return view('indexLink', ["data" => $data]);
    }

    public function delete($id) {
        Link::destroy($id);

        return redirect()->route('indexLink');
    }

    public function store() {

    }
}
