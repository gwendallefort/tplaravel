<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    public function link()
    {
        return $this->belongsTo('App\Models\Link')->withDefault();
    }
}
