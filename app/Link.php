<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public function categorie()
    {
        return $this->hasOne('App\Models\Categorie')->withDefault();
    }
}
