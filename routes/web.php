<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/links', "LinkController@index")->name("indexLink");
Route::post('/deleteLink/{id}', "LinkController@delete")->name("deleteLink");
Route::post('/createLink', "LinkController@store");


